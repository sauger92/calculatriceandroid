package threadslifecycle.android.fr.asynchronouscalculator;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * Created by Simon on 30/01/2018.
 */

public class ClientTask extends AsyncTask<String,Void,Double>
{
    Socket s;

    String message;
    Context c;
    Handler h = new Handler();

    ClientTask(Context c)
    {
        this.c=c;

    }


    @Override
    protected Double doInBackground(String... params) {
Double result = Double.valueOf(0);
        try {
            message = params[0];
            boolean b = false;
            double op1=0;
            double op2=0;
            char signe=' ';
            String buf1 = "";
            String buf2 = "";

            for (int x = 0; x < message.length(); x++)
            {
                if(b)
                {
                    buf2 = (String.valueOf(buf2)+(message.charAt(x)));
                }
                if((message.charAt(x)=='+'||message.charAt(x)=='*'||message.charAt(x)=='-'||message.charAt(x)=='/') && b!=true)
                {
                    b = true;
                    signe = message.charAt(x);
                }
                if (!b)
                {
                    buf1 = (String.valueOf(buf1)+(message.charAt(x)));
                }

            }
         op1 = Double.parseDouble(buf1);
            op2 = Double.parseDouble(buf2);


            s = new Socket("192.168.43.14",4444);

            DataOutputStream dos = new DataOutputStream(s.getOutputStream());
            //DataInputStream dis = new DataInputStream(s.getInputStream());

            //pw = new PrintWriter(dis);
            dos.writeDouble(op1);
            dos.writeChar(signe);
            dos.writeDouble(op2);
            System.out.println("op1"+op1);
            System.out.println("op"+ signe);
            System.out.println("op2"+op2);

            dos.flush();
            DataInputStream dis = new DataInputStream(s.getInputStream());
            result =dis.readDouble();
            h.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(c,"Message sent",Toast.LENGTH_LONG).show();

                }
            });
            dos.close();

        }catch(IOException e)
        {
            e.printStackTrace();
        }
        return result;

    }
}
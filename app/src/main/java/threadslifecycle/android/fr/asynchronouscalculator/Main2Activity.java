package threadslifecycle.android.fr.asynchronouscalculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

public class Main2Activity extends AppCompatActivity {
    Toolbar mytoolbar;
    Button button;
    EditText editText;
    private String pageWeb;
    private WebView webView;
    Intent gameActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        mytoolbar = (Toolbar) findViewById(R.id.my_toolbar2);
        setSupportActionBar(mytoolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        String op = (String) getIntent().getSerializableExtra("operation");
        String res = (String) getIntent().getSerializableExtra("resultat");
        TextView operation = (TextView) findViewById(R.id.operation);
        TextView resultat = (TextView) findViewById(R.id.resultat);
        resultat.setText(res);
        operation.setText(op);
        button = (Button) findViewById(R.id.Button);
        editText = (EditText) findViewById(R.id.EditText);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                gameActivity = new Intent(Main2Activity.this, Main3Activity.class);


                gameActivity.putExtra("lien",String.valueOf(editText.getText()));
                startActivity(gameActivity);
            }
        });
    }




}

package threadslifecycle.android.fr.asynchronouscalculator;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.nio.charset.IllegalCharsetNameException;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
    public TextView resultat;
    TextView operation;
    Toolbar mytoolbar;
    ListView sampleList;
    private ActionBar actionBar;
    Intent gameActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultat = (TextView) findViewById(R.id.resultat);

        mytoolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mytoolbar);
        //ActionBar ab = getSupportActionBar();
        //ab.setDisplayHomeAsUpEnabled(true);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                gameActivity = new Intent(this, Main2Activity.class);
                operation = (TextView) findViewById(R.id.operation);
                gameActivity.putExtra("operation", operation.getText());
                gameActivity.putExtra("resultat", resultat.getText());
                startActivity(gameActivity);
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

   /* public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_main_horizontal);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main);

        }


       //setContentView(R.layout.activity_main);
    }*/
    public void myClickHandler(View view) throws ExecutionException, InterruptedException {

         operation = (TextView) findViewById(R.id.operation);


        
        switch (view.getId()) {

            case R.id.button1:
                operation.setText(operation.getText()+"1");
                break;

            case R.id.button2:
                operation.setText(operation.getText()+"2");
                break;

            case R.id.button3:
                operation.setText(operation.getText()+"3");
                break;

            case R.id.button4:
                operation.setText(operation.getText()+"4");
                break;

            case R.id.button5:
                operation.setText(operation.getText()+"5");
                break;

            case R.id.button6:
                operation.setText(operation.getText()+"6");
                break;

            case R.id.button7:
                operation.setText(operation.getText()+"7");
                break;

            case R.id.button8:
                operation.setText(operation.getText()+"8");
                break;

            case R.id.button9:
                operation.setText(operation.getText()+"9");
                break;

            case R.id.button0:
                operation.setText(operation.getText()+"0");
                break;

            case R.id.buttonPlus:
                operation.setText(operation.getText()+"+");
                break;

            case R.id.buttonMoins:
                operation.setText(operation.getText()+"-");
                break;

            case R.id.buttonFois:
                operation.setText(operation.getText()+"*");
                break;
            case R.id.buttonDivise:
                operation.setText(operation.getText()+"/");
                break;

            case R.id.buttonSuppr:
                operation.setText("");
                break;

            case R.id.buttonEgal:
                send_data();

                break;






        }
    }


    public void send_data() throws ExecutionException, InterruptedException {
        ClientTask ct = new ClientTask(this);
        ct.execute(operation.getText().toString());
        double result = ct.get();
        resultat.setText(String.valueOf(result));

    }
}
